package dbAccessClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import com.mysql.cj.api.jdbc.Statement;

import connection.DBConnection;
import model.Product;

public class ProductDAO extends AbstractDAO<Product> {
	
	public Product findProductById(int id) {
		return findById(id);
	}
	
	public void insertProduct(Product product) {
		Connection connection = DBConnection.getConnection();
		String insertQuery = "INSERT INTO `songbirdbakery`.`products` (`productName`, `description`, `price`, `onStock`) VALUES (?, ?, ?, ?);";
		PreparedStatement statement = null;
		ResultSet rs = null;
		
		try {
			statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, product.getProductName());
			statement.setString(2, product.getDescription());
			statement.setInt(3, product.getPrice());
			statement.setInt(4, product.getOnStock());
			statement.executeUpdate();
			
			rs = statement.getGeneratedKeys();
			if (rs.next()) 
			{
				rs.getInt(1);
			}
			System.out.println("Insert Complete");
		} catch (SQLException e) {
			
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			DBConnection.close(rs);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
	
	public void updateProduct(Product product) {
		Connection connection = DBConnection.getConnection();
		String updateQuery = "UPDATE `songbirdbakery`.`products` SET `productName`=?, `description`=?, `price`=?, `onStock`=? WHERE `id`=?;";
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(updateQuery);
			statement.setString(1, product.getProductName());
			statement.setString(2, product.getDescription());
			statement.setInt(3, product.getPrice());
			statement.setInt(4, product.getOnStock());
			statement.setInt(5, product.getId());
			statement.executeUpdate();
			
			System.out.println("Update Complete");
		} catch (SQLException e) {
			
			LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
	
	public void deleteProduct(Product product) {
		Connection connection = DBConnection.getConnection();
		String updateQuery = "DELETE FROM `songbirdbakery`.`products` WHERE `id`=?;";
		PreparedStatement statement = null;
		
		try {
			statement = connection.prepareStatement(updateQuery);
			statement.setInt(1, product.getId());
			statement.executeUpdate();
			
			System.out.println("Delete Complete");
		} catch (SQLException e) {
			
			LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
}
