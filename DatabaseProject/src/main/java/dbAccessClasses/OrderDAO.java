package dbAccessClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import com.mysql.cj.api.jdbc.Statement;

import connection.DBConnection;
import model.OrderTable;

public class OrderDAO extends AbstractDAO<OrderTable> {

	public OrderTable findOrderById(int id) {
		return findById(id);
	}

	public void insertOrder(OrderTable order) {
		Connection connection = DBConnection.getConnection();
		String insertQuery = "INSERT INTO `songbirdbakery`.`orders` (`customerId`, `notes`, `productId`, `quantity`) VALUES (?, ?, ?, ?);";
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			statement.setInt(1, order.getCustomerId());
			statement.setString(2, order.getNotes());
			statement.setInt(3, order.getProductId());
			statement.setInt(4, order.getQuantity());
			statement.executeUpdate();

			rs = statement.getGeneratedKeys();
			if (rs.next()) {
				rs.getInt(1);
			}
			System.out.println("Insert Complete");
		} catch (SQLException e) {

			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			DBConnection.close(rs);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}

	public void deleteOrder(OrderTable order) {
		Connection connection = DBConnection.getConnection();
		String updateQuery = "DELETE FROM `songbirdbakery`.`orders` WHERE `id`=?;";
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(updateQuery);
			statement.setInt(1, order.getId());
			statement.executeUpdate();

			System.out.println("Delete Complete");
		} catch (SQLException e) {

			LOGGER.log(Level.WARNING, "OrdersDAO:update " + e.getMessage());
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}

}
