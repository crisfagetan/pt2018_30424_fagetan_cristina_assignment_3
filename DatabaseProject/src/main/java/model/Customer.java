package model;

public class Customer {

	private int id;
	private String name;
	private String address;
	private String email;
	private String phoneNumber;
	private String city;
	private boolean addToMailingList;

	public Customer() {
		// super();
	}

	public Customer(int id, String name, String address, String email, String phoneNumber, String city,
			boolean addToMailingList) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.city = city;
		this.addToMailingList = addToMailingList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public boolean getAddToMailingList() {
		return addToMailingList;
	}

	public void setAddToMailingList(boolean addToMailingList) {
		this.addToMailingList = addToMailingList;
	}

}
