package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainFrame extends JFrame {

	private JLabel loginLabel = new JLabel("Log in as:");
	private JLabel orLabel = new JLabel("or");

	private JButton customerButton = new JButton("Customer");
	private JButton adminButton = new JButton("Admin");

	public MainFrame() {

		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(null);
		this.add(firstPanel);

		loginLabel.setBounds(150, 30, 150, 50);
		firstPanel.add(loginLabel);
		loginLabel.setFont(loginLabel.getFont().deriveFont(30.0f));

		customerButton.setBounds(260, 110, 150, 40);
		firstPanel.add(customerButton);
		customerButton.setFont(customerButton.getFont().deriveFont(18.0f));
		customerButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				CustomersFrame customersFrame = new CustomersFrame();
				// customersFrame.loadCustomers();
				customersFrame.setVisible(true);
			}
		});

		orLabel.setBounds(210, 110, 100, 40);
		firstPanel.add(orLabel);
		orLabel.setFont(orLabel.getFont().deriveFont(20.0f));

		adminButton.setBounds(30, 110, 150, 40);
		firstPanel.add(adminButton);
		adminButton.setFont(adminButton.getFont().deriveFont(18.0f));
		adminButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				AdminFrame adminFrame = new AdminFrame();
				adminFrame.loadCustomers();
				adminFrame.setVisible(true);
			}
		});

		this.setTitle("Login");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(450, 300);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setContentPane(firstPanel);
		firstPanel.setBackground(new Color(230, 255, 255));
	}

}
