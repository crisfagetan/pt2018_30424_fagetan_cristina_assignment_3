package view;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JComboBox;

import connection.DBConnection;

public class PopulateCombo extends JComboBox {
	private Connection connection = null;
	private PreparedStatement statement = null;

	public PopulateCombo() {
		super();

		initComponents();

	}

	private void initComponents() {

		try {

			connection = DBConnection.getConnection();
			statement = connection.prepareStatement("SELECT id FROM customers");
			loadCombobox();

		} catch (Exception sqle) {
			System.out.println(sqle);
		}

	}

	public void loadCombobox() {

		this.removeAllItems();
		this.addItem("Please Select...");

		try {

			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				this.addItem(rs.getString("id"));
			}

		} catch (SQLException sqle) {
			System.out.println(sqle);
		}

	}

}