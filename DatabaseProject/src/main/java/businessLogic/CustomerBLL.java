package businessLogic;

import java.util.List;

import dbAccessClasses.CustomerDAO;
import model.Customer;

public class CustomerBLL {
	CustomerDAO dao = new CustomerDAO();

	public Customer findCustomerById(int id) {
		Customer customer = dao.findCustomerById(id);
		return customer;
	}

	public List<Customer> findAllCustomers() {
		return dao.findAll();
	}

	public void addCustomer(String name, String address, String mail, String phone, String city, boolean mailingList) {
		Customer customer = new Customer(0, name, address, mail, phone, city, mailingList);
		dao.insertCustomer(customer);
	}

	public void updateCustomer(int id, String name, String address, String mail, String phone, String city,
			boolean mailingList) {
		Customer customer = new Customer(id, name, address, mail, phone, city, mailingList);
		dao.updateCustomer(customer);
	}

	public void deleteCustomer(int id) {
		Customer customer = new Customer(id, " ", " ", " ", " ", " ", false);
		dao.deleteCustomer(customer);
	}
}
