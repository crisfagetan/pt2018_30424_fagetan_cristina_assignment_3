package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
	private static final Logger LOGGER = Logger.getLogger(DBConnection.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/songbirdbakery?useSSL=false"; // connection
	private static final String USER = "root";
	private static final String PASS = "root";

	private static DBConnection singleInstance = new DBConnection();

	// the constructor
	private DBConnection() {
		try {
			Class.forName(DRIVER);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	// creating a new connection
	private Connection createConnection() {
		Connection myConn = null;
		try {
			myConn = DriverManager.getConnection(DBURL, USER, PASS);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ERROR: trying to connect to the database");
			e.printStackTrace();
		}
		return myConn;
	}

	public static Connection getConnection() {
		return singleInstance.createConnection();
	}

	// closing a connection
	public static void close(Connection myConn) {
		if (myConn != null) {
			try {
				myConn.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ERROR: trying to close the connection");
			}
		}
	}

	// closing a statement
	public static void close(Statement myStm) {
		if (myStm != null)
			try {
				myStm.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ERROR: trying to close the statement");
			}
	}

	// closing the result set
	public static void close(ResultSet myRs) {
		if (myRs != null)
			try {
				myRs.close();
			} catch (SQLException e) {
				LOGGER.log(Level.WARNING, "ERROR: trying to close the result set");
			}
	}
}
