package dbAccessClasses;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import com.mysql.cj.api.jdbc.Statement;

import connection.DBConnection;
import model.Customer;

public class CustomerDAO extends AbstractDAO<Customer> {

	public Customer findCustomerById(int id) {
		return findById(id);
	}

	public void insertCustomer(Customer customer) {
		Connection connection = DBConnection.getConnection();
		String insertQuery = "INSERT INTO `songbirdbakery`.`customers` (`name`, `address`, `email`, `phoneNumber`, `city`, `addToMailingList`) VALUES (?, ?, ?, ?, ?, ?);";
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, customer.getName());
			statement.setString(2, customer.getAddress());
			statement.setString(3, customer.getEmail());
			statement.setString(4, customer.getPhoneNumber());
			statement.setString(5, customer.getCity());
			statement.setBoolean(6, customer.getAddToMailingList());
			statement.executeUpdate();

			rs = statement.getGeneratedKeys();
			if (rs.next()) {
				rs.getInt(1);
			}
			System.out.println("Insert Complete");
		} catch (SQLException e) {

			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			DBConnection.close(rs);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}

	public void updateCustomer(Customer customer) {
		Connection connection = DBConnection.getConnection();
		String updateQuery = "UPDATE `songbirdbakery`.`customers` SET `name`=?, `address`=?, `email`=?, `phoneNumber`=?, `city`=?, `addToMailingList`=? WHERE `id`=?;";
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(updateQuery);
			statement.setString(1, customer.getName());
			statement.setString(2, customer.getAddress());
			statement.setString(3, customer.getEmail());
			statement.setString(4, customer.getPhoneNumber());
			statement.setString(5, customer.getCity());
			statement.setBoolean(6, customer.getAddToMailingList());
			statement.setInt(7, customer.getId());
			statement.executeUpdate();

			System.out.println("Update Complete");
		} catch (SQLException e) {

			LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}

	public void deleteCustomer(Customer customer) {
		Connection connection = DBConnection.getConnection();
		String updateQuery = "DELETE FROM `songbirdbakery`.`customers` WHERE `id`=?;";
		PreparedStatement statement = null;

		try {
			statement = connection.prepareStatement(updateQuery);
			statement.setInt(1, customer.getId());
			statement.executeUpdate();

			System.out.println("Delete Complete");
		} catch (SQLException e) {

			LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
		} finally {
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
	}
}
