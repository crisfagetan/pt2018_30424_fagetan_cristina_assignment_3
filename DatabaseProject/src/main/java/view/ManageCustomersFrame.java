package view;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableModel;

import businessLogic.CustomerBLL;
import model.Customer;

public class ManageCustomersFrame extends JFrame {

	private CustomerBLL customerManager;
	TableModel tableModel;
	private JTable table;

	private JTextField id;
	private JTextField nameEdit;
	private JTextField addressEdit;
	private JTextField mailEdit;
	private JTextField phoneEdit;
	private JTextField cityEdit;
	private JTextField mailingListEdit;
	private JLabel idLabel = new JLabel("id");
	private JLabel nameLabel = new JLabel("name");
	private JLabel addressLabel = new JLabel("address");
	private JLabel mailLabel = new JLabel("mail");
	private JLabel phoneLabel = new JLabel("phone number");
	private JLabel cityLabel = new JLabel("city");
	private JLabel mailingListLabel = new JLabel("add to mailing list?");
	private JButton addButton = new JButton("ADD");
	private JButton updateButton = new JButton("UPDATE");
	private JButton deleteButton = new JButton("DELETE");

	public ManageCustomersFrame() {
		//
		customerManager = new CustomerBLL();
		tableModel = TableModelCreator.createTableModel(Customer.class, customerManager.findAllCustomers());

		// top
		JPanel topPanel = new JPanel();
		FlowLayout topLayout = new FlowLayout();
		topPanel.setLayout(topLayout);
		topPanel.add(addButton);
		topPanel.add(updateButton);
		topPanel.add(deleteButton);
		topPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(topPanel, BorderLayout.NORTH);

		// center
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		// bottom
		JPanel bottomPanel = new JPanel();
		FlowLayout bottomLayout = new FlowLayout();
		bottomPanel.setLayout(bottomLayout);
		id = new JTextField();
		id.setColumns(5);
		// id.setEditable(false);;
		nameEdit = new JTextField();
		nameEdit.setColumns(20);
		addressEdit = new JTextField();
		addressEdit.setColumns(20);
		mailEdit = new JTextField();
		mailEdit.setColumns(20);
		phoneEdit = new JTextField();
		phoneEdit.setColumns(20);
		cityEdit = new JTextField();
		cityEdit.setColumns(20);
		mailingListEdit = new JTextField();
		mailingListEdit.setColumns(20);
		bottomPanel.add(id);
		bottomPanel.add(nameEdit);
		bottomPanel.add(addressEdit);
		bottomPanel.add(mailEdit);
		bottomPanel.add(phoneEdit);
		bottomPanel.add(cityEdit);
		bottomPanel.add(mailingListEdit);
		bottomPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);
		/*
		 * table.getSelectionModel().addListSelectionListener(new
		 * ListSelectionListener() {
		 * 
		 * public void valueChanged(ListSelectionEvent event) { int rowIndex =
		 * event.getFirstIndex(); id.setText((String)table.getValueAt(rowIndex, 0));
		 * nameEdit.setText((String)table.getValueAt(rowIndex, 1));
		 * addressEdit.setText((String)table.getValueAt(rowIndex, 2));
		 * mailEdit.setText((String)table.getValueAt(rowIndex, 3));
		 * phoneEdit.setText((String)table.getValueAt(rowIndex, 4));
		 * cityEdit.setText((String)table.getValueAt(rowIndex, 5));
		 * mailingListEdit.setText((String)table.getValueAt(rowIndex, 6)); } });
		 */

		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				customerManager.addCustomer(nameEdit.getText(), addressEdit.getText(), mailEdit.getText(),
						phoneEdit.getText(), cityEdit.getText(), Boolean.parseBoolean(mailingListEdit.getText()));
				// loadCustomers();
			}
		});

		updateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				customerManager.updateCustomer(Integer.parseInt(id.getText()), nameEdit.getText(),
						addressEdit.getText(), mailEdit.getText(), phoneEdit.getText(), cityEdit.getText(),
						Boolean.parseBoolean(mailingListEdit.getText()));
				// loadCustomers();
			}
		});

		deleteButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				customerManager.deleteCustomer(Integer.parseInt(id.getText()));
				remove(Integer.parseInt(id.getText()));
				// loadCustomers();
			}
		});

		this.setTitle("Customer Manager");
		this.setSize(2000, 700);
		this.setLocationRelativeTo(null);

	}
	/*
	 * public void loadCustomers() { tableModel.load(); }
	 * 
	 * 
	 * private class CustomerTableModel extends AbstractTableModel {
	 * ArrayList<String[]> rows; // will hold String[] objects . . . int colCount;
	 * String[] headers = { "ID", "Name", "Address", "Mail", "Phone", "City",
	 * "Add to mailing list?" };
	 * 
	 * public CustomerTableModel() { rows = new ArrayList<String[]>(); }
	 * 
	 * public String getColumnName(int i) { return headers[i]; }
	 * 
	 * public int getColumnCount() { return headers.length; }
	 * 
	 * public int getRowCount() { return rows.size(); }
	 * 
	 * public Object getValueAt(int row, int col) { return ((String[])
	 * rows.get(row))[col]; }
	 * 
	 * 
	 * public void load() { try { rows.clear(); List<Customer> customers =
	 * customerManager.findAllCustomers(); for (Iterator<Customer> iterator =
	 * customers.iterator(); iterator.hasNext();) { Customer customer =
	 * iterator.next(); String[] row = new String[7]; row[0] =
	 * Integer.toString(customer.getId()); row[1] = customer.getName(); row[2] =
	 * customer.getAddress(); row[3] = customer.getEmail(); row[4] =
	 * customer.getPhoneNumber(); row[5] = customer.getCity(); row[6] =
	 * Boolean.toString(customer.getAddToMailingList()); rows.add(row); }
	 * fireTableChanged(null); } catch (Exception e) { rows = new
	 * ArrayList<String[]>(); e.printStackTrace(); } }
	 */
}
