package view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.ResultSet;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import businessLogic.CustomerBLL;
import businessLogic.OrderBLL;
import businessLogic.ProductBLL;
import model.Customer;
import model.Product;

public class OrdersFrame extends JFrame {

	private ProductBLL productsManager;
	private OrderBLL ordersManager;
	private JTable productTable;
	private JTable orderTable;
	ProductsTableModel productTableModel;
	OrdersTableModel orderTableModel;
	private String productId;

	private JLabel idLabel = new JLabel("ID:");
	private JLabel nameLabel = new JLabel("Customer:");
	private JLabel notesLabel = new JLabel("Notes:");
	private JLabel quantityLabel = new JLabel("Quantity:");
	private JTextField customerId;
	private JTextField customerName;
	private JTextField customerNotes;
	private JTextField productQuantity;
	private JButton add = new JButton("Add");
	private JButton delete = new JButton("Delete");
	private JButton pay = new JButton("Pay");

	public OrdersFrame() {
		//
		productsManager = new ProductBLL();
		ordersManager = new OrderBLL();

		// top
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		JPanel customerPanel = new JPanel();
		customerPanel.setLayout(new BorderLayout());
		FlowLayout topLayout = new FlowLayout();
		customerPanel.setLayout(topLayout);
		customerId = new JTextField();
		customerId.setColumns(5);
		customerId.setEditable(false);
		customerName = new JTextField();
		customerName.setColumns(50);
		customerName.setEditable(false);
		customerPanel.add(idLabel);
		customerPanel.add(customerId);
		customerPanel.add(nameLabel);
		customerPanel.add(customerName);
		customerPanel.add(add);
		customerPanel.add(delete);
		customerPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(customerPanel, BorderLayout.NORTH);
		JPanel notesPanel = new JPanel();
		notesPanel.setLayout(topLayout);
		customerNotes = new JTextField();
		customerNotes.setColumns(50);
		notesPanel.add(notesLabel);
		notesPanel.add(customerNotes);
		productQuantity = new JTextField();
		productQuantity.setColumns(5);
		productQuantity.setText("1");
		notesPanel.add(quantityLabel);
		notesPanel.add(productQuantity);
		notesPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		topPanel.add(notesPanel, BorderLayout.CENTER);
		add(topPanel, BorderLayout.NORTH);
		// center
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(topLayout);
		productTableModel = new ProductsTableModel();
		productTable = new JTable(productTableModel);
		JScrollPane scrollPane = new JScrollPane(productTable);
		centerPanel.add(scrollPane);

		orderTableModel = new OrdersTableModel();
		orderTable = new JTable(orderTableModel);
		JScrollPane scrollPaneOrders = new JScrollPane(orderTable);
		centerPanel.add(scrollPaneOrders);
		centerPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(centerPanel, BorderLayout.CENTER);
		// bottom
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(topLayout);
		bottomPanel.add(pay);
		bottomPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);

		productTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				int rowIndex = event.getFirstIndex();
				productId = ((String) productTableModel.getValueAt(rowIndex, 0));

			}
		});

		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				Product product = new Product();
				product = productsManager.findProductById(Integer.parseInt(productId));
				Component frame = null;
				if (product.getOnStock() > Integer.parseInt(productQuantity.getText())) {
					ordersManager.addOrder(Integer.parseInt(customerId.getText()), customerNotes.getText(),
							Integer.parseInt(productId), Integer.parseInt(productQuantity.getText()));
					product.setOnStock(product.getOnStock() - Integer.parseInt(productQuantity.getText()));
				} else
					JOptionPane.showMessageDialog(frame, "Under Stock. Please select a smaller amount!", "Inane error",
							JOptionPane.ERROR_MESSAGE);
			}
		});

		pay.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					MessageFormat headerFormat = new MessageFormat("All Products");
					MessageFormat footerFormat = new MessageFormat("- {0} -");
					productTable.print(JTable.PrintMode.FIT_WIDTH, headerFormat, footerFormat);
					productTable.print();
				} catch (PrinterException pe) {
					System.err.println("Error printing: " + pe.getMessage());
				}
			}
		});

		setSize(2000, 700);
		setLocationRelativeTo(null);
	}

	public void getClient(int id) {
		Customer customer = new Customer();
		CustomerBLL c = new CustomerBLL();
		customerId.setText(Integer.toString(id));
		customer = c.findCustomerById(id);
		customerName.setText(customer.getName());

	}

	public void loadProducts() {
		productTableModel.load();
	}

	private class ProductsTableModel extends AbstractTableModel {
		ArrayList<String[]> rows; // will hold String[] objects . . .
		int colCount;
		String[] headers = { "ID", "Name", "Description", "Price" };

		public ProductsTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				List<Product> products = productsManager.findAllProducts();
				for (Iterator<Product> iterator = products.iterator(); iterator.hasNext();) {
					Product product = iterator.next();
					String[] row = new String[4];
					row[0] = Integer.toString(product.getId());
					row[1] = product.getProductName();
					row[2] = product.getDescription();
					row[3] = Integer.toString(product.getPrice());
					rows.add(row);
				}
				fireTableChanged(null); // notify everyone that we have a new table.
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}

	}

	public void loadOrders() {
		orderTableModel.load();
	}

	private class OrdersTableModel extends AbstractTableModel {
		ArrayList<String[]> rows; // will hold String[] objects . . .
		String[] headers = { "ID", "Name", "Quantity", "Total" };

		public OrdersTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				Customer customer = new Customer();
				customer.setId(Integer.parseInt(customerId.getText()));
				ResultSet orders = ordersManager.findAllOrders(customer);
				while (orders.next()) {
					String[] row = new String[4];
					row[0] = Integer.toString(orders.getInt(1));
					row[1] = orders.getString(2);
					row[2] = Integer.toString(orders.getInt(3));
					row[3] = Integer.toString(orders.getInt(4));
					rows.add(row);
					System.out.println(Integer.toString(orders.getInt(1)) + "  " + orders.getString(2) + "  "
							+ Integer.toString(orders.getInt(3)) + Integer.toString(orders.getInt(4)));
				}
				fireTableChanged(null);
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}

	}
}
