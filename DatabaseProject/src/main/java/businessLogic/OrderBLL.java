package businessLogic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import connection.DBConnection;
import dbAccessClasses.OrderDAO;
import dbAccessClasses.ProductDAO;
import model.Customer;
import model.OrderTable;
import model.Product;

public class OrderBLL {
	OrderDAO dao = new OrderDAO();
	ProductDAO productDao = new ProductDAO();
	int total;

	public OrderTable findOrderById(int id) {
		return dao.findById(id);
	}

	public ResultSet findAllOrders(Customer customer) {
		Connection connection = DBConnection.getConnection();
		String findAllOrdersQuery = "SELECT orderid, productname, quantity, total FROM `songbirdbakery`.`seeorders` WHERE `customerId`=?;";
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			statement = connection.prepareStatement(findAllOrdersQuery);
			statement.setInt(1, customer.getId());
			rs = statement.executeQuery();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(rs);
			DBConnection.close(statement);
			DBConnection.close(connection);
		}
		return rs;
	}

	public void addOrder(int customerId, String notes, int productId, int quantity) {
		OrderTable order = new OrderTable(0, customerId, notes, productId, quantity);
		Product product = productDao.findById(productId);
		product.setOnStock(product.getOnStock() - quantity);
		dao.insertOrder(order);
		productDao.updateProduct(product);

	}

}
