package businessLogic;

import java.util.List;

import dbAccessClasses.ProductDAO;
import model.Product;

public class ProductBLL {
	ProductDAO dao = new ProductDAO();

	public Product findProductById(int id) {
		return dao.findById(id);
	}

	public List<Product> findAllProducts() {
		return dao.findAll();
	}

	public void addProduct(String name, String description, int price, int onStock) {
		Product product = new Product(0, name, description, price, onStock);
		dao.insertProduct(product);
	}

	public void updateProduct(int id, String name, String description, int price, int onStock) {
		Product product = new Product(id, name, description, price, onStock);
		dao.updateProduct(product);
	}

	public void deleteProduct(int id) {
		Product product = new Product(id, "", "", 0, 0);
		dao.deleteProduct(product);
	}

}
