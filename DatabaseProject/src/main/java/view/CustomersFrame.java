package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CustomersFrame extends JFrame {

	String selected;
	private JLabel selectLabel = new JLabel("Select  a customer");

	private JButton selectButton = new JButton("Select");
	private JButton manageButton = new JButton("Manage Customers");
	private PopulateCombo clientCombo = new PopulateCombo();

	public CustomersFrame() {

		JPanel firstPanel = new JPanel();
		firstPanel.setLayout(null);
		this.add(firstPanel);

		selectLabel.setBounds(90, 30, 300, 50);
		firstPanel.add(selectLabel);
		selectLabel.setFont(selectLabel.getFont().deriveFont(30.0f));

		manageButton.setBounds(130, 200, 180, 40);
		firstPanel.add(manageButton);
		manageButton.setFont(manageButton.getFont().deriveFont(16.0f));
		manageButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				ManageCustomersFrame manageCustomers = new ManageCustomersFrame();
				// manageCustomers.loadCustomers();
				manageCustomers.setVisible(true);
			}
		});

		clientCombo.setBounds(30, 110, 180, 40);
		firstPanel.add(clientCombo);
		clientCombo.setFont(clientCombo.getFont().deriveFont(18.0f));

		selectButton.setBounds(250, 110, 150, 40);
		firstPanel.add(selectButton);
		selectButton.setFont(selectButton.getFont().deriveFont(18.0f));
		selectButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				OrdersFrame orderFrame = new OrdersFrame();
				selected = (String) clientCombo.getSelectedItem();
				Component frame = null;
				if (selected != "Please Select...") {
					orderFrame.getClient(Integer.parseInt(selected));
					orderFrame.loadProducts();
					orderFrame.setVisible(true);
				} else
					JOptionPane.showMessageDialog(frame, "Please select a customer!", "Inane error",
							JOptionPane.ERROR_MESSAGE);

			}
		});

		this.setTitle("Customer Select");
		this.setSize(450, 300);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setContentPane(firstPanel);
		firstPanel.setBackground(new Color(230, 255, 255));
	}

}
