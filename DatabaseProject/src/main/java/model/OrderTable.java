package model;

public class OrderTable {
	private int id;
	private int customerId;
	private String notes;
	private int productId;
	private int quantity;

	public OrderTable() {
		super();
	}

	public OrderTable(int id, int customerId, String notes, int productId, int quantity) {
		super();
		this.id = id;
		this.customerId = customerId;
		this.notes = notes;
		this.productId = productId;
		this.quantity = quantity;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

}
