package model;

public class SeeOrders {
	private int orderid;
	private int customerid;
	private String productName;
	private int quantity;
	private int total;

	public SeeOrders() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getOrderid() {
		return orderid;
	}

	public int getCustomerid() {
		return customerid;
	}

	public String getProductName() {
		return productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getTotal() {
		return total;
	}

}
