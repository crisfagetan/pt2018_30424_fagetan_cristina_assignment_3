package view;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;

import businessLogic.ProductBLL;
import model.Product;

public class AdminFrame extends JFrame {

	private ProductBLL productManager;
	private JTable table;
	ProductTableModel tableModel;
	private JTextField id;
	private JTextField nameEdit;
	private JTextField descriptionEdit;
	private JTextField priceEdit;
	private JTextField onStockEdit;
	private JButton addButton = new JButton("ADD");
	private JButton updateButton = new JButton("UPDATE");
	private JButton deleteButton = new JButton("DELETE");

	public AdminFrame() {
		//
		productManager = new ProductBLL();

		// top
		JPanel topPanel = new JPanel();
		FlowLayout topLayout = new FlowLayout();
		topPanel.setLayout(topLayout);
		topPanel.add(addButton);
		topPanel.add(updateButton);
		topPanel.add(deleteButton);
		topPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(topPanel, BorderLayout.NORTH);

		// center
		tableModel = new ProductTableModel();
		table = new JTable(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		add(scrollPane, BorderLayout.CENTER);
		// bottom
		JPanel bottomPanel = new JPanel();
		FlowLayout bottomLayout = new FlowLayout();
		bottomPanel.setLayout(bottomLayout);
		id = new JTextField();
		id.setColumns(25);
		id.setEditable(false);
		;
		nameEdit = new JTextField();
		nameEdit.setColumns(25);
		descriptionEdit = new JTextField();
		descriptionEdit.setColumns(40);
		priceEdit = new JTextField();
		priceEdit.setColumns(25);
		onStockEdit = new JTextField();
		onStockEdit.setColumns(25);
		bottomPanel.add(id);
		bottomPanel.add(nameEdit);
		bottomPanel.add(descriptionEdit);
		bottomPanel.add(priceEdit);
		bottomPanel.add(onStockEdit);
		bottomPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		add(bottomPanel, BorderLayout.SOUTH);
		table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				int rowIndex = event.getFirstIndex();
				id.setText((String) tableModel.getValueAt(rowIndex, 0));
				nameEdit.setText((String) tableModel.getValueAt(rowIndex, 1));
				descriptionEdit.setText((String) tableModel.getValueAt(rowIndex, 2));
				priceEdit.setText((String) tableModel.getValueAt(rowIndex, 3));
				onStockEdit.setText((String) tableModel.getValueAt(rowIndex, 4));
			}
		});

		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				productManager.addProduct(nameEdit.getText(), descriptionEdit.getText(),
						Integer.parseInt(priceEdit.getText()), Integer.parseInt(onStockEdit.getText()));
				loadCustomers();
			}
		});

		updateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				productManager.updateProduct(Integer.parseInt(id.getText()), nameEdit.getText(),
						descriptionEdit.getText(), Integer.parseInt(priceEdit.getText()),
						Integer.parseInt(onStockEdit.getText()));
				loadCustomers();
			}
		});

		deleteButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				productManager.deleteProduct(Integer.parseInt(id.getText()));
				loadCustomers();
			}
		});

		this.setTitle("Admin - Products Manager");
		this.setSize(2000, 700);
		this.setLocationRelativeTo(null);

	}

	public void loadCustomers() {
		tableModel.load();
	}

	private class ProductTableModel extends AbstractTableModel {
		ArrayList<String[]> rows;
		int colCount;
		String[] headers = { "ID", "Name", "Description", "Price", "On Stock" };

		public ProductTableModel() {
			rows = new ArrayList<String[]>();
		}

		public String getColumnName(int i) {
			return headers[i];
		}

		public int getColumnCount() {
			return headers.length;
		}

		public int getRowCount() {
			return rows.size();
		}

		public Object getValueAt(int row, int col) {
			return ((String[]) rows.get(row))[col];
		}

		public void load() {
			try {
				rows.clear();
				List<Product> products = productManager.findAllProducts();
				for (Iterator<Product> iterator = products.iterator(); iterator.hasNext();) {
					Product product = iterator.next();
					String[] row = new String[5];
					row[0] = Integer.toString(product.getId());
					row[1] = product.getProductName();
					row[2] = product.getDescription();
					row[3] = Integer.toString(product.getPrice());
					row[4] = Integer.toString(product.getOnStock());
					rows.add(row);
				}
				fireTableChanged(null); // notify everyone that we have a new table.
			} catch (Exception e) {
				rows = new ArrayList<String[]>();
				e.printStackTrace();
			}
		}

	}

}
